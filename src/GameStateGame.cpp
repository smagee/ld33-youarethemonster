#include "GameStateGame.h"
#include "GameStatePaused.h"
#include <iostream>

GameStateGame::GameStateGame(Game* game)
{
	game_ = game;
	level_ = new Level();

	sf::Vector2f pos = sf::Vector2f(game_->window_.getSize());
	view_.setSize(pos);
	pos *= 0.5f;
	view_.setCenter(pos);
}

GameStateGame::~GameStateGame()
{
	//dtor
}

void GameStateGame::checkState()
{

}

void GameStateGame::pollEvent()
{
	checkState();

	sf::Event event;
	while(game_->window_.pollEvent(event))
	{
		switch(event.type)
		{
		case sf::Event::Closed:
			game_->window_.close();
			break;
		case sf::Event::KeyPressed:
			if(event.key.code == sf::Keyboard::Escape)
				pauseGame();
			else if(event.key.code == sf::Keyboard::F1)
			{
				if(level_->showCollisions_)
				{
					level_->showCollisions_ = false;
					std::cout << "Collisions Off" << std::endl;
				}
				else
				{
					level_->showCollisions_ = true;
					std::cout << "Collisions On" << std::endl;
				}
			}
			break;
		case sf::Event::MouseButtonPressed:
			if(event.key.code == sf::Mouse::Left)
			{
				level_->player()->fireDart(sf::Mouse::getPosition(game_->window_));//(level_->player()->getDirection(sf::Mouse::getPosition(game_->window_)));
			}
			break;
		default:
			break;
		}
	}
}

void GameStateGame::checkInput()
{
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		level_->player()->moveUp();
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		level_->player()->moveDown();
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		level_->player()->moveLeft();
	}
	if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		level_->player()->moveRight();
	}
}

void GameStateGame::updateObjects()
{
	level_->update();
}

void GameStateGame::updateAI()
{

}

void GameStateGame::checkCollisions()
{
	level_->updateCollisions();
}

void GameStateGame::updateWindow()
{
	game_->window_.setView(view_);
	game_->window_.clear(sf::Color::White);
	level_->draw(game_->window_);
	game_->window_.display();
}

void GameStateGame::draw()
{

}

void GameStateGame::pauseGame()
{
	game_->pushState(new GameStatePaused(game_, this));
}
