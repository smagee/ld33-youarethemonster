#include "Level.h"
#include "SleepDart.h"

Level::Level()
{
	player_ = new Player(this, sf::Vector2f(400, 300));
	enemys_.push_back(new Enemy(this, sf::Vector2f(400, 150)));

	texture_.loadFromFile("assets/Map.png");
	map_.setTexture(texture_);

	//sf::FloatRect bound1 = sf::FloatRect(510, 0, 100, 30);
	//levelBounds_.push_back(bound1);

	showCollisions_ = false;
}

Level::~Level()
{
	//dtor
}

void Level::fireDart(sf::Vector2i mousePos)
{
	player_->fireDart(mousePos);
}

Player* Level::player()
{
	return player_;
}

void Level::update()
{
	player_->update();
	for(std::list<Enemy*>::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		(*it)->update();
	}
}

void Level::updateCollisions()
{
	SleepDart* dart = player_->getDart();
	for(std::list<Enemy*>::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		if((*it)->getGlobalBounds().intersects(dart->getGlobalBounds()))
			(*it)->sleep();
	}
}

void Level::draw(sf::RenderWindow& window)
{
	window.draw(map_);
	window.draw(*player());
	for(std::list<Enemy*>::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		window.draw(*(*it));
	}

	if(showCollisions_)
	{
		drawCollisions(window);
	}
}

void Level::drawCollisions(sf::RenderWindow& window)
{
	sf::FloatRect boundingBox;
	sf::RectangleShape collision = sf::RectangleShape();
	collision.setFillColor(sf::Color::Transparent);
	collision.setOutlineColor(sf::Color::Red);
	collision.setOutlineThickness(1.0f);
	std::vector<Entity*> entitys;

	entitys.push_back(player_);
	for(std::list<Enemy*>::iterator it = enemys_.begin(); it != enemys_.end(); ++it)
	{
		entitys.push_back((*it));
	}

	for(std::vector<Entity*>::iterator it = entitys.begin(); it != entitys.end(); ++it)
	{
		boundingBox = (*it)->getGlobalBounds();
		collision.setSize(sf::Vector2f(boundingBox.width, boundingBox.height));
		collision.setPosition(boundingBox.left, boundingBox.top);
		window.draw(collision);
	}

	for(std::vector<sf::FloatRect>::iterator it = levelBounds_.begin(); it != levelBounds_.end(); ++it)
	{

		boundingBox = (*it);
		collision.setSize(sf::Vector2f(boundingBox.width, boundingBox.height));
		collision.setPosition(boundingBox.left, boundingBox.top);
		window.draw(collision);
	}
}
