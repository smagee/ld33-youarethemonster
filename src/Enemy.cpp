#include "Enemy.h"

Enemy::Enemy(Level* level, sf::Vector2f position)
{
	level_ = level;
	texture_.loadFromFile("assets/EnemySprite.png");
	entity_.setTexture(texture_);
	setSpriteDown();
	entity_.setPosition(position);
	entity_.scale(2, 2);

	sleeping = false;
}

Enemy::~Enemy()
{

}

void Enemy::update()
{

}

void Enemy::sleep()
{
	sleeping = true;
	setSpriteSleep();
}

void Enemy::moveUp()
{
	setSpriteUp();
	entity_.move(0.0f, -1.0f*movement_);
}

void Enemy::moveDown()
{
	setSpriteDown();
	entity_.move(0.0f, 1.0f*movement_);
}

void Enemy::moveLeft()
{
	setSpriteLeft();
	entity_.move(-1.0f*movement_, 0.0f);
}

void Enemy::moveRight()
{
	setSpriteRight();
	entity_.move(1.0f*movement_, 0.0f);
}

void Enemy::setSpriteUp()
{
	entity_.setTextureRect(sf::IntRect(0, 0, 18, 31));
}

void Enemy::setSpriteDown()
{
	entity_.setTextureRect(sf::IntRect(0, 0, 18, 31));
}

void Enemy::setSpriteLeft()
{
	entity_.setTextureRect(sf::IntRect(0, 0, 18, 31));
}

void Enemy::setSpriteRight()
{
	entity_.setTextureRect(sf::IntRect(0, 0, 18, 31));
}

void Enemy::setSpriteSleep()
{
	entity_.setTextureRect(sf::IntRect(0, 32, 31, 18));
}

void Enemy::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(entity_, states);
}
