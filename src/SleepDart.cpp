#include "SleepDart.h"

SleepDart::SleepDart(sf::Sprite dart)
{
	dart_ = dart;
	alive_ = true;
}

SleepDart::~SleepDart()
{

}

void SleepDart::fire(sf::Vector2f direction, sf::Vector2f pos)
{
	direction_ = direction;
	dart_.setPosition(pos);
}

void SleepDart::update()
{
	dart_.move(direction_.x * 5.0f, direction_.y * 5.0f);
}

bool SleepDart::canFire()
{
	return alive_;
}

sf::FloatRect SleepDart::getGlobalBounds()
{
	return dart_.getGlobalBounds();
}

void SleepDart::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(dart_, states);
}
