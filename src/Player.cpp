#include "Player.h"

Player::Player(Level* level, sf::Vector2f position)
{
	level_ = level;
	texture_.loadFromFile("assets/PlayerSprite.png");
	entity_.setTexture(texture_);
	setSpriteUp();
	entity_.setPosition(position);
	entity_.scale(0.5f, 0.5f);
	movement_ = 3.0f;

	dartSprite_.setTexture(texture_);
	dartSprite_.setTextureRect(sf::IntRect(0, 32, 3, 8));
	dart_ = new SleepDart(dartSprite_);
}

Player::~Player()
{
	//dtor
}

void Player::update()
{
	dart_->update();
}

bool Player::isTouching(Enemy* enemy)
{
	if(entity_.getGlobalBounds().intersects(enemy->getGlobalBounds()))
		return true;
	return false;
}

void Player::fireDart(sf::Vector2i mousePos)
{
	if(dart_->canFire())
	{
		sf::Vector2f direction = getDirection(mousePos);
		dart_->fire(direction, entity_.getPosition());
	}
}

SleepDart* Player::getDart()
{
	return dart_;
}

void Player::moveUp()
{
	setSpriteUp();
	entity_.move(0.0f, -1.0f*movement_);
}

void Player::moveDown()
{
	setSpriteDown();
	entity_.move(0.0f, 1.0f*movement_);
}

void Player::moveLeft()
{
	setSpriteLeft();
	entity_.move(-1.0f*movement_, 0.0f);
}

void Player::moveRight()
{
	setSpriteRight();
	entity_.move(1.0f*movement_, 0.0f);
}

void Player::setSpriteUp()
{
	entity_.setTextureRect(sf::IntRect(0, 0, 17, 27));
}

void Player::setSpriteDown()
{
	entity_.setTextureRect(sf::IntRect(32, 0, 17, 27));
}

void Player::setSpriteLeft()
{
	entity_.setTextureRect(sf::IntRect(64, 0, 27, 17));
}

void Player::setSpriteRight()
{
	entity_.setTextureRect(sf::IntRect(96, 0, 27, 17));
}

sf::Vector2f Player::getDirection(sf::Vector2i mousePos)
{
	sf::Vector2f myPos = entity_.getPosition();
	sf::Vector2f direction = sf::Vector2f(mousePos.x - myPos.x, mousePos.y - myPos.y);
	float hyp = sqrtf(direction.x * direction.x +  direction.y * direction.y);
	direction.x /= hyp;
	direction.y /= hyp;
	return direction;
}

void Player::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(entity_, states);
	if(dart_->alive_ == true)
		target.draw(*dart_, states);
}
