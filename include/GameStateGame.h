#ifndef GAMESTATEGAME_H
#define GAMESTATEGAME_H

#include <SFML/Graphics.hpp>

#include "GameState.h"
#include "Level.h"

class GameStatePaused;

class GameStateGame : public GameState
{
	public:
		GameStateGame(Game* game);
		virtual ~GameStateGame();

		void checkState();
		void pollEvent();
		void checkInput();
		void updateObjects();
		void updateAI();
		void checkCollisions();
		void updateWindow();
		void draw();
		void pauseGame();
	protected:
	private:
		Level* level_;

		sf::View view_;
};

#endif // GAMESTATEGAME_H
