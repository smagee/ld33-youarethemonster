#ifndef SLEEPDART_H
#define SLEEPDART_H

#include <list>

#include <SFML/Graphics.hpp>

#include "Enemy.h"

class SleepDart : public sf::Drawable, public sf::Transformable
{
	public:
		SleepDart(sf::Sprite dart);
		virtual ~SleepDart();

		void fire(sf::Vector2f direction, sf::Vector2f pos);
		void update();
		bool canFire();
		bool alive_;

		sf::FloatRect getGlobalBounds();
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;

		sf::Vector2f direction_;

		sf::Sprite dart_;
};

#endif // SLEEPDART_H
