#ifndef ENTITY_H
#define ENTITY_H

#include <SFML/Graphics.hpp>

class Entity
{
	public:
		Entity();
		virtual ~Entity();

		float getMovementSpeed();
		int getHealth();
		sf::FloatRect getGlobalBounds();
	protected:
		float movement_;
		int health_;

		sf::Sprite entity_;
		sf::Texture texture_;
	private:
};

#endif // ENTITY_H
