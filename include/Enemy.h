#ifndef ENEMY_H
#define ENEMY_H

#include <SFML/Graphics.hpp>

#include "Entity.h"
#include "Level.h"

class Level;

class Enemy : public Entity, public sf::Drawable, public sf::Transformable
{
	public:
		Enemy(Level* level, sf::Vector2f position);
		virtual ~Enemy();

		void moveUp();
		void moveDown();
		void moveLeft();
		void moveRight();
		void sleep();
		void update();
		bool intersects();
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		void setSpriteUp();
		void setSpriteDown();
		void setSpriteLeft();
		void setSpriteRight();
		void setSpriteSleep();

		bool sleeping;

		Level* level_;
};

#endif // ENEMY_H
