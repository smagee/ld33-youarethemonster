#ifndef LEVEL_H
#define LEVEL_H

#include <list>

#include <SFML/Graphics.hpp>

#include "Player.h"
#include "Enemy.h"

class Player;
class Enemy;

class Level
{
	public:
		Level();
		virtual ~Level();

		void fireDart(sf::Vector2i mousePos);
		void update();
		void updateCollisions();
		void draw(sf::RenderWindow& window);

		Player* player();

		bool showCollisions_;
	protected:
	private:
		void drawCollisions(sf::RenderWindow& window);

		Player* player_;
		std::list<Enemy*> enemys_;
		std::vector<sf::FloatRect> levelBounds_;

		sf::Sprite map_;
		sf::Texture texture_;
};

#endif // LEVEL_H
