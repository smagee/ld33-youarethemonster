#ifndef PLAYER_H
#define PLAYER_H

#include <vector>

#include <SFML/Graphics.hpp>

#include "Entity.h"
#include "Enemy.h"
#include "SleepDart.h"
#include "Level.h"

class Level;
class Enemy;
class SleepDart;

class Player : public Entity, public sf::Drawable, public sf::Transformable
{
	public:
		Player(Level* level, sf::Vector2f position);
		virtual ~Player();

		void moveUp();
		void moveDown();
		void moveLeft();
		void moveRight();
		void update();

		// player actions
		bool isTouching(Enemy* enemy);
		void fireDart(sf::Vector2i mousePos);
		sf::Vector2f getDirection(sf::Vector2i mousePos);

		SleepDart* getDart();
	protected:
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		void setSpriteUp();
		void setSpriteDown();
		void setSpriteLeft();
		void setSpriteRight();

		SleepDart* dart_;

		sf::Sprite dartSprite_;
		Level* level_;
};

#endif // PLAYER_H
